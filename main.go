package main

import (
	"fmt"
	"log"
	pb "user-service/proto/user"

	"github.com/micro/go-micro"
)

func main() {

	db, err := CreateConnection()
	defer db.Close()

	if err != nil {
		log.Fatalf("Could not connect to Db: %v", err)
	}

	db.AutoMigrate(&pb.User{})
	repo := &UserRepository{db}

	srv := micro.NewService(micro.Name("user"))
	srv.Init()

	pb.RegisterUserServiceHandler(srv.Server(), &handler{repo})

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}

}
