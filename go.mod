module user-service

go 1.13

require (
	github.com/golang/protobuf v1.3.2
	github.com/google/uuid v1.1.1
	github.com/jinzhu/gorm v1.9.11
	github.com/micro/go-micro v1.18.0
)
