package main

import (
	pb "user-service/proto/user"

	"github.com/jinzhu/gorm"
)

type Repository interface {
	GetAll() ([]*pb.User, error)
	Get(id string) (*pb.User, error)
	Create(user *pb.User) error
	GetByEmailAndPassword(user *pb.User) (*pb.User, error)
}

type UserRepository struct {
	db *gorm.DB
}

// GetAll ...
func (rp *UserRepository) GetAll() ([]*pb.User, error) {
	var users []*pb.User
	if err := rp.db.Find(&users).Error; err != nil {
		return nil, err
	}

	return users, nil
}

// Get ...
func (rp *UserRepository) Get(id string) (*pb.User, error) {
	var user *pb.User
	user.Id = id
	if err := rp.db.First(&user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

// Create ...
func (rp *UserRepository) Create(user *pb.User) error {
	if err := rp.db.Create(user).Error; err != nil {
		return err
	}
	return nil
}

// GetByEmailAndPassword ...
func (rp *UserRepository) GetByEmailAndPassword(user *pb.User) (*pb.User, error) {
	if err := rp.db.First(&user).Error; err != nil {
		return nil, err
	}

	return user, nil
}
