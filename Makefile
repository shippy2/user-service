build:
	protoc -I proto proto/user/user.proto --micro_out=proto --go_out=proto

docker:
	docker build -t user-service .
