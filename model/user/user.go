package user

import (
	pb "user-service/proto/user"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// BeforeCreate ...
func (model *pb.User) BeforeCreate(scope *gorm.Scope) error {
	uuid := uuid.New()
	return scope.SetColumn("Id", uuid.String())
}
