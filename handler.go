package main

import (
	"context"
	pb "user-service/proto/user"
)

type handler struct {
	repo Repository
	//tokenService Authable
}

// Get ...
func (h *handler) Get(ctx context.Context, req *pb.User, res *pb.Response) error {
	user, err := h.repo.Get(req.Id)
	if err != nil {
		return err
	}

	res.User = user
	return nil
}

// GetAll ...
func (h *handler) GetAll(ctx context.Context, req *pb.Requst, res *pb.Response) error {
	users, err := h.repo.GetAll()
	if err != nil {
		return err
	}
	res.Users = users
	return nil
}

// Auth ...
func (h *handler) Auth(ctx context.Context, req *pb.User, res *pb.Token) error {
	_, err := h.repo.GetByEmailAndPassword(req)
	if err != nil {
		return err
	}

	res.Token = "token"
	return nil
}

// Create ...
func (h *handler) Create(ctx context.Context, req *pb.User, res *pb.Response) error {
	if err := h.repo.Create(req); err != nil {
		return err
	}

	res.User = req
	return nil
}

// ValidateToken ...
func (h *handler) ValidateToken(ctx context.Context, req *pb.Token, res *pb.Token) error {
	return nil
}
